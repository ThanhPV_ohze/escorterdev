/**
 * Created by ThanhPham on 4/3/2017.
 */
// import * as Express from "express";
// import * as Path from "path";
import * as restify from "restify";
import * as SocketIO from "socket.io";
import * as Passport from "passport";
import { Strategy }  from "passport-facebook"
// import * as fs from "fs";
var myStrategy = {
    FACEBOOK_APP_ID : "422164331477815",
    FACEBOOK_APP_SECRET : "0e27c54634ee3dae1ce86577ed67ec98",
    callbackURL : "",
    SERVER_PREFIX : "http://localhost",

    FB_LOGIN_PATH : "/api/facebook_login",
    FB_CALLBACK_PATH : "/api/facebook_callback"
};

export class Server {
    httpServer;
    communicateServer;
    fbPassport;

    //handler
    fb_login_handler;
    fb_callback_handler;
    fb_callback_handler2;

    // static app = Express();

    constructor() {
        // this.initExpressServer();
        // this.startListening();
        this.initRestifyServer();
    }

    initSocketIOServers = () => {
        this.communicateServer = SocketIO(this.httpServer.server);
    }

    initRestifyServer = () => {
        this.httpServer = restify.createServer();
        this.httpServer.use(restify.queryParser());

        this.httpServer.listen(process.env.port || process.env.PORT || 80 || 443 || 8080, () => {
            console.log('%s listening to %s', this.httpServer.name, this.httpServer.url);
        });



        this.httpServer.use(Passport.initialize());

        Passport.use(new Strategy({
            clientID : myStrategy.FACEBOOK_APP_ID,
            clientSecret : myStrategy.FACEBOOK_APP_SECRET,
            callbackURL : myStrategy.SERVER_PREFIX + myStrategy.FB_CALLBACK_PATH
        },
        (accessToken, refreshToken, profile, done) => {
            console.log('accessToken='+accessToken+' facebookId='+profile.id);
            return done(null, profile);
        }));

        this.initHandler();
        this.initGetRequest();
    }

    initHandler = () => {
        this.fb_login_handler = Passport
            .authenticate("facebook", {session : false});
        this.fb_callback_handler = Passport
            .authenticate("facebook", {session : false});
        this.fb_callback_handler2 = (req, res) => {
            console.log("Logged in!");
            console.dir(req.user);
            //send a response
            // res.send('Welcome' + req.user.toString());
        }
    }

    initGetRequest = () => {
        //Setting some get request event handlers
        // this.httpServer.get(/\/?.*/, restify.serveStatic({
        //     directory: './webpack',
        //     default: 'index.html'
        // })); //Work but still missing smthing

        this.httpServer.get(myStrategy.FB_LOGIN_PATH, this.fb_login_handler);
        this.httpServer.get(myStrategy.FB_CALLBACK_PATH, this.fb_callback_handler, this.fb_callback_handler2);

        /*
        *   Not working
        *   Don't ask me anything, i dunno either
         */

        // this.httpServer.get('/',(req, res, next) => {
        //     fs.readFile(
        //         __dirname + 'webpack/index.html', (err, data) => {
        //             if (err) {
        //                 next(err);
        //                 return;
        //             }
        //         }
        //     )
        //     res.setHeader('Content-Type', 'text/html');
        //     res.writeHead(200);
        //     // res.end(data);
        //
        // });
    };

    // initExpressServer = () => {
    //     // Server.app.get('/', (req, res) => {
    //     //    res.sendFile(Path.join(__dirname + '/html'));
    //     // });
    //     console.log("Start listening!");
    //     Server.app.use('/', Express.static(__dirname + '/webpack'));
    //
    //     // Server.app.all('/secret', (req, res, next) => {
    //     //     console.log('Accessing the secret section ...')
    //     //     next() // pass control to the next handler
    //     // })
    // }
    //
    // startListening = () => {
    //     Server.app.listen(8080);
    // }
}

new Server();