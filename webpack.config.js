/**
 * Created by cuongdd on 1/21/2017.
 */
var path = require('path'),
    webpack = require('webpack'),
    plugins = [],
    HtmlWebpackPlugin = require('html-webpack-plugin');

    plugins.push(new HtmlWebpackPlugin());
    // plugins.push(new webpack.optimize.UglifyJsPlugin(), new HtmlWebpackPlugin());

module.exports = {

    entry: {
        main: path.resolve("src/Main.ts")
    },
    devtool: "#source-map",
    output: {
        path: __dirname + "/webpack",
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {test: /.ts$/, loader: 'awesome-typescript-loader'}
        ]
    },
    plugins: plugins,
    resolve: {
        extensions: ['.ts', '.js', '.json']
    }
};
