"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by ThanhPham on 4/3/2017.
 */
// import * as Express from "express";
// import * as Path from "path";
var restify = require("restify");
var SocketIO = require("socket.io");
var Passport = require("passport");
var passport_facebook_1 = require("passport-facebook");
// import * as fs from "fs";

var myStrategy =
    {
    FACEBOOK_APP_ID: "422164331477815",
    FACEBOOK_APP_SECRET: "0e27c54634ee3dae1ce86577ed67ec98",
    callbackURL: "",

    SERVER_PREFIX: "http://escorter.hamic.club",
    FB_LOGIN_PATH: "/login/facebook",
    FB_CALLBACK_PATH: "/api/facebook_callback"
};

var Server = (function () {
    // static app = Express();
    function Server() {
        var _this = this;
        this.initSocketIOServers = function () {
            _this.communicateServer = SocketIO(_this.httpServer.server);
        };
        this.initRestifyServer = function () {
            _this.httpServer = restify.createServer();
            _this.httpServer.use(restify.queryParser());
            _this.httpServer.listen(process.env.port || process.env.PORT || 80 || 443 || 8080, function () {
                console.log('%s listening to %s', _this.httpServer.name, _this.httpServer.url);
            });
            _this.httpServer.use(Passport.initialize());
            Passport.use(new passport_facebook_1.Strategy({
                clientID: myStrategy.FACEBOOK_APP_ID,
                clientSecret: myStrategy.FACEBOOK_APP_SECRET,
                callbackURL: myStrategy.SERVER_PREFIX + myStrategy.FB_CALLBACK_PATH
            }, function (accessToken, refreshToken, profile, done) {
                console.log('accessToken=' + accessToken + ' facebookId=' + profile.id);
                return done(null, profile);
            }));
            _this.initHandler();
            _this.initGetRequest();
        };
        this.initHandler = function () {
            _this.fb_login_handler = Passport
                .authenticate("facebook", { session: false });
            _this.fb_callback_handler = Passport
                .authenticate("facebook", { session: false });
            _this.fb_callback_handler2 = function (req, res) {
                console.log("Logged in!");
                console.dir(req.user);
                //send a response
                // res.send('Welcome' + req.user.toString());
            };
        };
        this.initGetRequest = function () {
            //Setting some get request event handlers
            // this.httpServer.get(/\/?.*/, restify.serveStatic({
            //     directory: './webpack',
            //     default: 'index.html'
            // })); //Work but still missing smthing
            _this.httpServer.get(myStrategy.FB_LOGIN_PATH, _this.fb_login_handler);
            _this.httpServer.get(myStrategy.FB_CALLBACK_PATH, _this.fb_callback_handler, _this.fb_callback_handler2);
            /*
            *   Not working
            *   Don't ask me anything, i dunno either
             */
            // this.httpServer.get('/',(req, res, next) => {
            //     fs.readFile(
            //         __dirname + 'webpack/index.html', (err, data) => {
            //             if (err) {
            //                 next(err);
            //                 return;
            //             }
            //         }
            //     )
            //     res.setHeader('Content-Type', 'text/html');
            //     res.writeHead(200);
            //     // res.end(data);
            //
            // });
        };
        // this.initExpressServer();
        // this.startListening();
        this.initRestifyServer();
    }
    return Server;
}());
exports.Server = Server;
new Server();
