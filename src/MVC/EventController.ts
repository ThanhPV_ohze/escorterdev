import {Main} from "../Main";
import {isNullOrUndefined} from "util";
/**
 * Created by ThanhPham on 3/24/2017.
 */
export class EventController {

    constructor (public View:Main) { };

    startLoginScreen = () => {
        this.View.showLoginScreen();
    }

    loginFrameClosed = (idToken?) => {
        if (!isNullOrUndefined(idToken)) {
            this.startMainScreen(idToken);
        } else {
            this.loginInterrupted();
        }
        console.log("Login popup closed!");
    }

    startMainScreen = (idToken) => {
        console.log("Triggered startMainScreen event!");
    }

    loginInterrupted = () => {

    }

}