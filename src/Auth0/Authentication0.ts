/**
 * Created by ThanhPham on 3/24/2017.
 */
// import * as auth0 from "auth0-js";
import Auth0Lock from "auth0-lock";
import {eventCtl} from "../Main";

export class AUTH0LIBRARY {
    public static readonly CLIENTID = 'dcyqzMLJUVrgFXDlFVcbOjraeLMl9wzs';
    public static readonly DOMAIN = 'thanhmsp.au.auth0.com';
}

export class Authentication0 {
    // private webAuth0;
    private lock = new Auth0Lock(AUTH0LIBRARY.CLIENTID, AUTH0LIBRARY.DOMAIN, {
        language: 'vi',
        auth: {
            redirect: false,
            sso : false
        }
    });


    constructor () {

        this.lock.on("authenticated", function(authResult) {
            localStorage.setItem("idToken", authResult.idToken);
            localStorage.setItem("result", JSON.stringify(authResult));
            this.lock.hide();
        });

        this.lock.on("authorization_error", (authResult) => {
           console.log(authResult)
        });

        this.lock.on("hide", () => {
            // Authentication0.lock.getProfile(localStorage.getItem('idToken'), (error, profile) => {
            //     console.log(JSON.stringify(profile));
            // });
            eventCtl.loginFrameClosed(localStorage.getItem('idToken'));
        });

        this.lock.on("show",
            () => {

            });

    }

    facebookPopupLogin = () => {
        this.lock.show();
    }
}