/**
 * Created by ThanhPham on 3/24/2017.
 */
import "socket.io-client";

export class SocketConnector {

    private socketConnector;

    private eventController;

    initConnection = () => {
        this.socketConnector = io('http://localhost:3000');

        this.socketConnector.on('connect', () => {
           console.log('initConnection completed!');
        });
    }

    constructor() {
        this.initConnection();
    }

    emitEvent = (event:string, parameters) => {
        this.socketConnector.emit(event, parameters);
    }

    emitEventAndAddHandler = (event:string, parameters, callback) => {
        this.socketConnector.emit(event, parameters);
        this.socketConnector.on(event, callback);
    }

    emitEventHandler = (event:string, callback) => {
        this.socketConnector.on(event, callback);
    }
}