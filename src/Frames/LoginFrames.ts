/**
 * Created by ThanhPham on 3/24/2017.
 */

import "pixi.js";
// import * as Auth0 from "auth0-js";

import {FBLoginButton} from "../Controls/FBLoginButton";
import {Authentication0} from "../Auth0/Authentication0";

export class LoginFrames extends PIXI.Container {
    private fbLoginButton : FBLoginButton = new FBLoginButton();
    private authentication0 : Authentication0 = new Authentication0();
    // public webAuth;

    constructor() {
        super();
        this.initComponents();
    }

    initComponents =() => {
        this.addChild(this.fbLoginButton);

        this.fbLoginButton.on("pointerdown", this.authentication0.facebookPopupLogin);
    }
}