/**
 * Created by ThanhPham on 3/24/2017.
 */
import {LoginFrames} from "./Frames/LoginFrames";
import {isNullOrUndefined} from "util";
import {EventController} from "./MVC/EventController";

export class Main {
    private app;

    static LoginFrame;

    static EventCtl;

    constructor () {
        this.initComponents();
    }

    initLoginFrame = () => {
        Main.LoginFrame = new LoginFrames;
        this.app.stage.addChild(Main.LoginFrame);
    }

    showLoginScreen = () => {
        if (isNullOrUndefined(Main.LoginFrame)) {
            this.initLoginFrame();
        }
        Main.LoginFrame.visible = true;
    }

    destroyLoginScreen = () => {

    }

    initComponents = () => {
        this.initPixiApp();

        this.initLoginFrame();
        Main.LoginFrame.visible = false;

    }

    initPixiApp = () => {
        this.app = new PIXI.Application(800, 600, {transparent: true});
        document.body.appendChild(this.app.view);
    }

}
export var view = new Main();
export var eventCtl = new EventController(view);
eventCtl.startLoginScreen();
