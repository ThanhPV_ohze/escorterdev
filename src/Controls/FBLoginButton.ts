/**
 * Created by ThanhPham on 3/24/2017.
 */
import "pixi.js"
import {LoginObjectInterface} from "./Interface/LoginObjectInterface";
import {ControlsLibrary} from "./ControlsLibrary";

export class FBLoginButton extends PIXI.Sprite implements LoginObjectInterface{
    loginRequest: (userData?) => {

    };

    constructor () {
        super(PIXI.Texture.fromImage(ControlsLibrary.FB_BUTTON_ICON));
        this.interactive = true;
        this.buttonMode = true;
    }


}